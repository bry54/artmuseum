import {FETCH_GALLERY, UPDATE_GLOBAL_CONTROLS} from '../utilities/actionTypes';
import * as Font from 'expo-font';
import {Asset} from 'expo-asset';
import {appImages} from "../../assets/images";
import axios from "axios";

const defaultGalleryUri = 'https://api.harvardartmuseums.org/object?apikey=f317d3b0-a5b2-48f1-9723-6c0b2798bc47';

export const updateGlobalControls = (controlObject) => dispatch => {
    dispatch({
        type: UPDATE_GLOBAL_CONTROLS,
        payload: controlObject
    });
};

export const fetchGallery = (galleryUri = defaultGalleryUri) => async dispatch => {
    dispatch(updateGlobalControls({key: 'fetching', value: true}))
    axios.get( galleryUri )
        .then( (response) => {
            dispatch({
                type: FETCH_GALLERY,
                payload: response.data
            });
            dispatch(updateGlobalControls({key: 'fetching', value: false}))
        })
        .catch( (error) => {
            // handle error
            console.log(error);
        })
}
