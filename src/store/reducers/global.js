import {FETCH_GALLERY, UPDATE_GLOBAL_CONTROLS} from "../utilities/actionTypes";

const initialState = {
    resourcesCached: false,
    info: null,
    gallery: null,
    fetching: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_GLOBAL_CONTROLS:{
            const controlObject = action.payload;
            const key = controlObject.key;
            const value = controlObject.value;

            return {
                ...state,
                [key] : value
            };
        }

        case FETCH_GALLERY:{
            const payload = action.payload;
            let gallery = state.gallery;

            if (!gallery)
                gallery = payload.records
            else {
                const newItems = payload.records
                newItems.forEach(item => {
                    if (gallery.find(_item => _item.id === item.id)){

                    } else {
                        gallery.push(item);
                    }
                })
            }

            return {
                ...state,
                gallery : gallery,
                info: payload.info
            };
        }

        default:
            return state;
    }
};

export {initialState, reducer};
