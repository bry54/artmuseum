import thunk from 'redux-thunk';
import {AsyncStorage} from "react-native";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {initialState as initialGlobalReducer, reducer as globalReducer} from '../../store/reducers/global'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const migrations = {
    0: (state) => {
        return {
            // reinitialize modified reducers.
            ...state,
            globalReducer: initialGlobalReducer,
        }
    },
};

const rootReducer = combineReducers({
    globalReducer: globalReducer,
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export { store };
