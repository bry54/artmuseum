import React from 'react';

import Colors from '../constants/ColorDefinitions';
import {Ionicons, MaterialCommunityIcons} from '@expo/vector-icons';

export default class TabBarIcon extends React.Component {
    render() {
        return (
            <MaterialCommunityIcons
                name={ this.props.name }
                size={24}
                style={{ marginBottom: 3, marginTop: 2 }}
                color={ this.props.focused ? Colors.tabs.tabIconSelected : Colors.tabs.tabIconDefault } />
        );
    }
}
