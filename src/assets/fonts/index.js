export default {
    b612MonoRegular: require('./B612Mono-Regular.ttf'),
    crimsonProRegular: require('./CrimsonPro-Regular.ttf'),
    greatVibesRegular: require('./GreatVibes-Regular.ttf'),
}
