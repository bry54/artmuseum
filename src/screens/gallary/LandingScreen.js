import React, {Component} from 'react';
import {StyleSheet, FlatList, View, Text, TouchableOpacity, ActivityIndicator, RefreshControl} from 'react-native';
import ColorDefinitions from "../../constants/ColorDefinitions";
import LayoutDefinitions from "../../constants/LayoutDefinitions";
import {connect} from "react-redux";
import { Image } from 'react-native-elements';
import {fetchGallery} from "../../store/actions/global";

class LandingScreen extends Component {

    componentDidMount() {
        this.props.fetchGallery()
    }

    render () {
        if (!this.props.gallery)
            return <ActivityIndicator />
        else
            return (
                <View style={{ }}>
                    <FlatList
                        refreshing={this.props.fetching}
                        refreshControl={
                            <RefreshControl refreshing={this.props.fetching} title={'Fetching art'} />
                        }
                        numColumns={2}
                        data={this.props.gallery}
                        renderItem={(item) => this.renderItem(item)}
                        keyExtractor={((item, index) => JSON.stringify(item.id))}
                        onEndReached={this.handleLoadMore}
                        onEndThreshold={0}
                    />
                </View>
            );
    }

    renderItem = ({item}) => {
        return(
            <View style={styles.galleryItem}>
                <TouchableOpacity
                    onPress={() => this._loadArtDetail(item)}
                    style={{  }}>
                    <View style={{height: 50, justifyContent: 'center'}}>
                        <Text numberOfLines={2} style={styles.imageTitle}>{item.title}</Text>
                    </View>
                    {item.primaryimageurl ?
                        <Image source={{uri: item.primaryimageurl}}
                               style={styles.artPreview}
                               PlaceholderContent={<ActivityIndicator />} />
                        :
                        <View style={{height: 200, justifyContent: 'center', borderTopColor: 'black', borderTopWidth: .5, borderBottomWidth: .5}}>
                            <Text style={{textAlign: 'center', fontFamily: 'alpha-regular', fontSize: 17}}>No Image</Text>
                        </View>
                    }
                    <View>
                        <Text style={styles.artOwner}>
                            {item.people ? item.people[0]["displayname"] : 'N/A'}
                        </Text>
                        <Text style={styles.artCulture}>
                            {item.people ? item.people[0]["culture"] : 'N/A'}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    _loadArtDetail = (item) => {
        const {navigation} = this.props;
        navigation.navigate('ArtDetail', {art: item})
    }

    handleLoadMore = () => {
        const nextUri = this.props.info.next;

        if (nextUri){
            this.props.fetchGallery(nextUri);
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding:10,
        backgroundColor: ColorDefinitions.mainBackground,
    },

    galleryItem: {
        width: (LayoutDefinitions.window.width/2)-10,
        borderRadius: 10,
        margin: 5,
        backgroundColor: '#ccc',
        elevation: 2
    },

    imageTitle: {
        textAlign: 'center',
        fontSize: 16,
        padding: 5,
        fontFamily: 'alpha-regular'
    },

    artPreview: {
        height: 200
    },

    artOwner: {
        textAlign: 'center',
        fontSize: 14,
        padding: 5,
        fontFamily: 'alpha-regular'
    },

    artCulture: {
        textAlign: 'center',
        fontSize: 13,
        padding: 5,
        fontFamily: 'alpha-regular'
    },

    specialsContainer: {
        width: LayoutDefinitions.window.width/2,
        height: 200,
        paddingHorizontal: 0,
    },

    title: {
        fontWeight: 'normal',
        color: ColorDefinitions.colorAccent,
        fontSize: 26,
        backgroundColor: 'rgba(255, 255, 255, 0.1)',
        textAlign: 'center',
        marginBottom: 16,
        fontFamily: 'alpha-regular',
        marginVertical: 100,
        marginHorizontal: 10,
        paddingHorizontal: 10,
        borderRadius: 50
    },
});

const mapStateToProps = (state) => {
    return {
        info: state.globalReducer.info,
        gallery: state.globalReducer.gallery,
        fetching: state.globalReducer.fetching
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchGallery: (galleryUri) => dispatch(fetchGallery(galleryUri))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
