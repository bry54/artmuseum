import React, {Component} from 'react';
import {ActivityIndicator, ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Button, Icon, Image} from "react-native-elements";
import * as Linking from 'expo-linking';
import {MaterialCommunityIcons} from "@expo/vector-icons";

class LandingScreen extends Component {

    render () {
        const {navigation, route} = this.props;
        const {art} = route.params;

        console.log(art)
        return (
            <ScrollView style={{}}>
                {art.primaryimageurl ?
                    <Image source={{uri: art.primaryimageurl}}
                           style={{ height: 200 }}
                           PlaceholderContent={<ActivityIndicator />} />
                    :
                    <View style={{height: 200, justifyContent: 'center', borderColor: 'darkgrey', borderTopWidth: .5, borderBottomWidth: .5, backgroundColor: 'darkgrey', elevation: 5}}>
                        <Text style={{textAlign: 'center', textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 25}}>No Image</Text>
                    </View>
                }

                <View style={{padding: 10, flexDirection: 'row', justifyContent: 'center'}}>
                    <Text style={{fontSize: 20, paddingVertical: 3, paddingHorizontal: 10, textTransform: 'uppercase', fontFamily: 'alpha-regular'}}>Art Info</Text>
                    <MaterialCommunityIcons
                        name={ art.verificationlevel ? 'check-circle' : 'close-circle' }
                        size={24}
                        style={{ marginBottom: 3, marginTop: 2 }}
                        color={ art.verificationlevel ? 'green' : 'red' } />
                </View>

                <View style={{padding: 10, flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <Text style={{fontSize: 17, textTransform: 'capitalize', fontFamily: 'alpha-regular'}}>{`Medium: ${art.medium}`}</Text>
                </View>

                <View style={{padding: 10, flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <Text style={{fontSize: 17, textTransform: 'capitalize', fontFamily: 'alpha-regular'}}>{`Division: ${art.division}`}</Text>
                </View>

                <View style={{padding: 10, flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <Text style={{fontSize: 17, textTransform: 'capitalize', fontFamily: 'alpha-regular'}}>Artists: </Text>
                    <View>
                        {art.people ? art.people.map((p, index)=> (<Text style={{fontSize: 17, fontFamily: 'alpha-regular'}} key={index.toString()}>- {p.displayname}</Text>)) : <Text>N/A</Text>}
                    </View>
                </View>

                <View style={{padding: 10, justifyContent: 'flex-start'}}>
                    <Text style={{fontSize: 17, textTransform: 'capitalize', fontFamily: 'alpha-regular'}}>{`Description: ${art.description ? art.description : 'N/A'}`}</Text>
                </View>

                <View style={{padding: 10, justifyContent: 'center'}}>
                    <Text style={{fontSize: 20, textTransform: 'capitalize', textAlign: 'center', paddingBottom: 5, fontFamily: 'alpha-regular'}}>Provenance</Text>
                    <Text style={{fontSize: 17, fontFamily: 'alpha-regular' }}>{art.provenance ? art.provenance : 'N/A' }</Text>
                </View>

                <View style={{padding: 10, justifyContent: 'center'}}>
                    <Button
                        onPress={() => Linking.openURL(art.url)}
                        title="See More Info"
                        titleStyle={{fontFamily: 'alpha-regular'}}
                    />
                </View>



            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
        justifyContent: 'center'
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
