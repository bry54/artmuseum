import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Avatar} from "react-native-elements";

class LandingScreen extends Component {

    render () {
        return (
            <View style={{flex: 1, alignItems: 'center', paddingVertical: 50}}>
                <View style={{flexDirection: 'row'}}>
                    <Avatar
                    overlayContainerStyle={{backgroundColor: ColorDefinitions.colorPrimary}}
                    containerStyle={{elevation: 5}}
                    size={"medium"}
                    icon={{name: 'apple', type: 'material-community', color: 'grey'}}
                    rounded/>
                    <Avatar
                    overlayContainerStyle={{backgroundColor: ColorDefinitions.colorPrimary}}
                    containerStyle={{elevation: 5}}
                    size={"medium"}
                    icon={{name: 'android', type: 'material-community', color: 'grey'}}
                    rounded/>
                </View>
                <Text style={{paddingVertical: 15, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                    Compatible with both android and ios.
                </Text>
                <Text style={{paddingVertical: 40, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                    An assessment application for gunionconsultancy.
                </Text>
                <Text style={{paddingVertical: 0, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18, textAlign: 'center'}}>
                    Created By
                </Text>
                <Text style={{fontFamily: 'handwriting', fontSize: 24, textAlign: 'center', paddingVertical: 20}}>Brian Paidamoyo Sithole</Text>
                <Text style={{paddingVertical: 0, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18}}>
                    Phone: +905338347917
                </Text>
                <Text style={{paddingVertical: 0, paddingHorizontal: 10,fontFamily: 'alpha-regular', fontSize: 18}}>
                    Email: brianacyth@live.co.uk
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
        justifyContent: 'center' //remove this line when creating content
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
