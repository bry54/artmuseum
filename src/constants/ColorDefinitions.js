const tintColor = '#b71c1c';

const colorSchemes = {
    tintColor: tintColor,
    errorBackground: '#ff0000',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',

    mainBackground: '#616161',
    colorPrimary:'#121218',
    colorPrimaryDark:'#121218',
    colorAccent:'#76FF03',

    primaryText:{
        shade0: '#76FF03',
    },

    secondaryText:{
        shade0: '#121218',
    },

    alternateText:{
        shade0: '#616161',
    },

    primaryBackground:{
        shade0: '#121218',
    },

    secondaryBackground:{
        shade0: '#76FF03',
    },

    alternateBackground:{
        shade0: '#616161',
    },

    tabs: {
        tabIconDefault: '#ccc',
        tabIconSelected: 'blue',
        tabBar: '#fefefe'
    }
};

export default colorSchemes;
