import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabBarIcon from "../components/TabBarIcon";
import GalleryNavigator from "./GalleryNavigator";
import AboutNavigator from "./AboutNavigator";

const Tab = createBottomTabNavigator();

export default () => {
    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: 'blue',
                inactiveTintColor: 'gray',
            }}>
            <Tab.Screen
                name="Home"
                component={GalleryNavigator}
                options={({ route }) => ({
                    tabBarLabel: 'Gallery',
                    tabBarIcon: ({ focused }) => <TabBarIcon name='image-album' focused={focused}/>
                })}
            />
            <Tab.Screen
                name="About"
                component={AboutNavigator}
                options={({ route }) => ({
                    tabBarLabel: 'About',
                    tabBarIcon: ({ focused }) => <TabBarIcon name='information-outline' focused={focused}/>
                })}
            />
        </Tab.Navigator>
    );
}
