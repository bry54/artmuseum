import { createStackNavigator } from '@react-navigation/stack';
import React from "react";
import GalleryLandingScreen  from "../screens/gallary/LandingScreen";
import ArtDetailScreen  from "../screens/gallary/ArtDetailScreen";

const GalleryStack = createStackNavigator();

export default () =>{
    return (
        <GalleryStack.Navigator>
            <GalleryStack.Screen
                name="Gallery"
                component={GalleryLandingScreen}
                options={{
                    title: 'Art Gallery',
                    headerTitleStyle: {fontFamily: 'alpha-regular',}
                }}/>
            <GalleryStack.Screen
                name="ArtDetail"
                component={ArtDetailScreen}
                options={{
                    title: 'Art Detail',
                    headerTitleStyle: {fontFamily: 'alpha-regular',}
                }}
            />
        </GalleryStack.Navigator>
    );
}
