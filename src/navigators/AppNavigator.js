import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
/*import SplashScreen from "../screens/SplashScreen";*/
import MainTabNavigator from "./MainTabNavigator";

const AppStack = createStackNavigator();

export default () => (
    <NavigationContainer>
        <AppStack.Navigator headerMode='none'>
            <AppStack.Screen name="Main" component={MainTabNavigator} />
        </AppStack.Navigator>
    </NavigationContainer>
)
