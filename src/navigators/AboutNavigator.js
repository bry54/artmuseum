import { createStackNavigator } from '@react-navigation/stack';
import React from "react";
import AboutScreen  from "../screens/about/LandingScreen";

const AboutNavigator = createStackNavigator();

export default () =>{
    return (
        <AboutNavigator.Navigator>
            <AboutNavigator.Screen
                name="AboutApp"
                component={AboutScreen}
                options={{
                    title: 'About App',
                    headerTitleStyle: {fontFamily: 'alpha-regular',}
                }}/>
        </AboutNavigator.Navigator>
    );
}
