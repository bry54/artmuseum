# Art Museum App

Assessment mobile app for gunionconsultancy.

# Features!

  - Listing of gallery items
  - Showing art detail

### Core Frameworks

Application was developed using the following tools/frameworks:

* React Native
* Redux
* React Navigation
* git vcs (Bitbucket)

### Installation

To run the demo application you will need to install expo client on your mobile device.

#### With Development Environment.
Be sure you have the following tools installed on your development environment:

* yarn or npm [Yarn](https://yarnpkg.com/) [npm](https://www.npmjs.com/get-npm)
* expo-cli [expo-cli](https://expo.io/tools)
* git vcs [Git](https://git-scm.com/)

```sh
$ git clone https://bry54@bitbucket.org/bry54/artmuseum.git
$ cd artmuseum
$ yarn install
$ expo start
```

#### Without Development Environment...

- Visit this URL [Expo](https://expo.io/tools) to install Expo.
- Visit this URL [ArtMuseum](https://expo.io/@bry54/projects/artmuseum) to preview Art Museum app.
