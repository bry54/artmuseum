import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { store } from './src/store/utilities/storeConfiguration';
import { registerRootComponent } from 'expo';

import App from './App';

export default class WrappedApp extends Component{
    render() {
        return (
            <Provider store={store}>
                <App/>
            </Provider>
        )
    }
}

registerRootComponent(WrappedApp);
