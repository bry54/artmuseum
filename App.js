import React, { PureComponent} from 'react';
import {StyleSheet, StatusBar, View} from 'react-native';
import * as Font from 'expo-font';
import {Ionicons, MaterialCommunityIcons} from '@expo/vector-icons';
import Colors from './src/constants/ColorDefinitions';
import 'react-native-gesture-handler'
import fonts from './src/assets/fonts';
import AppNavigator from "./src/navigators/AppNavigator";
import * as SplashScreen from 'expo-splash-screen';

import {updateGlobalControls} from "./src/store/actions/global";
import {connect} from "react-redux";

class App extends PureComponent {
    state = {
        fontsLoaded: false,
    };

    async componentDidMount() {
        try {
            await SplashScreen.preventAutoHideAsync();
        } catch (e) {
            console.warn(e);
        }
        await this.prepareResources();
    }

    prepareResources = async () => {
        try {
            await this.cacheFonts([MaterialCommunityIcons.font])
            await Font.loadAsync({
                'handwriting': fonts.greatVibesRegular,
                'alpha-regular': fonts.crimsonProRegular,
                'numeric-regular': fonts.b612MonoRegular
            }).then(res => this._updateState( 'fontsLoaded', true )
            ).catch(error => console.log(error));
        } catch (error) {
            console.log(error);
        } finally {
            this._updateState( 'fontsLoaded', true )
            await SplashScreen.hideAsync();
        }
    };

    cacheFonts = (fonts) => {
        return fonts.map(font => Font.loadAsync(font));
    };

    render() {
        const appIsReady = this.state.fontsLoaded;

        if (!appIsReady) {
            return null;
        }

        return (
            <View style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={Colors.colorPrimaryDark}/>
                <AppNavigator/>
            </View>
        );
    }

    _updateState = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        backgroundColor: Colors.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(App)
